import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  params;

  constructor(
    public router: Router,
    private actroute: ActivatedRoute,
    private keycloak: KeycloakService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.params = this.actroute.snapshot.url
    //console.log(this.params);

  }

  ngOnInit(): void {
    //console.log(this.actroute);

  }

  goToRoute(id) {
    this.router.navigate(['assets-overview']);

    // if(id == '') this.router.navigate(['assets-overview']);
    // else this.router.navigate(['assets-overview' , 'dashboards' , id]);

  }

  logout() {
    this.keycloak.logout()
  }
}
