import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, ActivatedRoute } from '@angular/router';

import { KeycloakAuthGuard } from 'keycloak-angular';
import { KeycloakService } from 'keycloak-angular';
import { AuthService } from '../guard/auth.guard.service';
// import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})


export class AuthGuard extends KeycloakAuthGuard {

  token;
  attributes;
  tags;

  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private authRightsServ: AuthService,

  ) {

    super(router, keycloak);

    // this.token = this.authRightsServ.getToken()
    // this.attributes = this.authRightsServ.getRights()

  }

  async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean | UrlTree> {

    route.data = { ...route.data, title: route.data?.title || 'Untitled' }; // Ensure title property is set

    if (!this.authenticated) {

      let redirect = window.location.origin + state.url;
      // if (environment.production) redirect = window.location.origin + '/fvt' + state.url;
      await this.keycloak.login({
        redirectUri: redirect,
      });


      if (route.params.id) {
        if (this.authenticated && this.authRightsServ.checkRights(route.params.id)) {
          return true
        }
        else {
          this.router.navigate(['cases-overview'], { relativeTo: this.route })
          this._snackBar.open('Access Denied. You are not allowed to access this case.', 'X', { duration: 5000, panelClass: 'errorClass', verticalPosition: 'top', horizontalPosition: 'center' })
          return false
        }


      } else return this.authenticated;



    }

    // Check that the user has priviledges for the specific service
    const requiredRoles = route.data['roles'] as Array<string>;
    if (requiredRoles && requiredRoles.length > 0){
      const hasRole = requiredRoles.some(role => this.keycloak.isUserInRole(role));
      if (!hasRole){
        this._snackBar.open('Access Denied. You do not have the required permissions to access this page.', 'X',
          { duration: 5000, panelClass: 'errorClass', verticalPosition: 'top', horizontalPosition: 'center' });
        return false;
      }
    }

    return this.authenticated;
  }
}
