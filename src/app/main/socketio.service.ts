import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  socket;
  private inMessage = new BehaviorSubject(undefined);
  private rabbitmqMessage = new BehaviorSubject(undefined);

  constructor() {

    if (environment.production) this.socket = io("http://132.227.122.23:30615/", { path: '/ws/socket.io' });
    else this.socket = io(environment.socket_host, { path: '/ws/socket.io' });

    this.socket.on('template', (data) => {
      this.inMessage.next(data);
    });

    this.socket.on('rabbitMQmessage', (data) => {
      this.rabbitmqMessage.next(data);
    });
  }

  rabbitmqMessages = () => {
    this.socket.connect();
    return this.rabbitmqMessage.asObservable();
  }

    //open socket + create observable
  liveData = () => {

    this.socket.connect();
    return this.inMessage.asObservable();
  }

   //close socket + clear observable
  disconSocket() {
    this.socket.disconnect();
    //this.socket.off('message')
    this.inMessage.next(undefined)
    this.rabbitmqMessage.next(undefined)
  }


}
