import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class NodejsDataService {

  // myhttpHeaders = new HttpHeaders().append('Content-Type', 'application/json').append("Authorization", "Basic " + btoa("admin:Pa55w0rd"));
  myhttpHeaders;


  constructor(private http_: HttpClient,
    private keycloak: KeycloakService
  ) {
  }


  // ----- NEMO -----
  public getWorkloads() {
    return this.http_.get<any>(environment.host + "/workload/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getWorkloadsMoca() {
    return this.http_.get<any>(environment.moca + "/workload/retrieve", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getWorkloadInstances() {
    return this.http_.get<any>(environment.host + "/workload/instance/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getWorkloadInstancesFiltered(fv) {
    let params = new HttpParams();
    params = params.set('workload_document', fv);

    return this.http_.get<any>(environment.host + "/workload/instance/", {params, headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public createWorkload(data) {
    return this.http_.post<any>(environment.host + "/workload/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public uploadWorkloadDocument(data) {
    return this.http_.post<any>(environment.host + "/workload/upload/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public renderWorkload(data, id) {
    return this.http_.post<any>(environment.host + "/workload/" + id + "/template/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public deleteWorkload(id) {
    return this.http_.delete<any>(environment.host + "/workload/" + id + "/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public deleteWorkloadInstance(id) {
    return this.http_.put<any>(environment.host + "/workload/instance/" + id + "/delete/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getInstanceManifests(id) {
    return this.http_.get<any>(environment.host + "/workload/instance/" + id + "/manifests/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getIntents() {
    return this.http_.get<any>(environment.host + "/intent/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getIntentTypes() {
    return this.http_.get<any>(environment.host + "/intent/types/", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public createIntent(data) {
    return this.http_.post<any>(environment.host + "/intent/template/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public actionIntent(id, data) {
    return this.http_.put<any>(environment.host + "/intent/" + id + "/action/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public changeTargetIntent(id, data){
    return this.http_.put<any>(environment.host + "/intent/" + id + "/target/", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public getClusters() {
    return this.http_.get<any>(environment.moca + "/cluster/retrieve", {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }

  public createCluster(data) {
    return this.http_.post<any>(environment.moca + "/cluster/register", data, {headers: this.myhttpHeaders })
    .toPromise().catch((error) => {
      const customError = { message: error };
      return customError;
    });
  }



  
  public getAssets(token, path, body) {
    return this.getWorkloads()


    return this.http_.post(environment.host + path + '/getAssets', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      console.log(environment.host + path + '/getAssets', body, { headers: this.myhttpHeaders });

      const customError = { message: error };
      return customError;
    })
  }




  // ----- End of NEMO -----





  public getToken() {
    let token = this.keycloak['_instance'].token

    this.myhttpHeaders = new HttpHeaders().append('Content-Type', 'application/json; charset=utf-8').append("Authorization", "Bearer " + token);
    return token
  }

  public getKeycloakId() {
    return this.keycloak['_userProfile'].attributes.elastic_id[0]
  }

  // public getAssets(token, path, body) {

  //   console.log('HERE', token);
  //   console.log(environment.host)

  //   return this.http_.post(environment.host + path + '/getAssets', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
  //     console.log(environment.host + path + '/getAssets', body, { headers: this.myhttpHeaders });

  //     const customError = { message: error };
  //     return customError;
  //   })
  // }

  public getPreconfView(asset, preconfid) {

    let url = 'assets/preconfViews/' + preconfid + '.json'
    return this.http_.get(url).toPromise()

  }

  public getUSB(path, body) {

    return this.http_.post(
      environment.host + path + '/getUSB', body, { headers: this.myhttpHeaders }
    ).toPromise()
      .catch((error) => {
        const customError = { message: 'An error occurred while fetching data' };
        return customError;
      })
  }

  public getAlerts(path, body) {


    return this.http_.post(
      environment.host + path + '/getAlerts', body, { headers: this.myhttpHeaders }
    ).toPromise()
      .catch((error) => {
        const customError = { message: 'An error occurred while fetching data' };
        return customError;
      })
  }


  public getMetrics(path, body) {

    return this.http_.post(environment.host + path + '/getMetrics', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }
  public getCommunication(path, body) {


    return this.http_.post(environment.host + path + '/getCommunication', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }
  public getProcesses(path, body) {

    return this.http_.post(environment.host + path + '/getProcesses', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }
  public getUncommonProcesses(path, body) {

    return this.http_.post(environment.host + path + '/getUncommonProcesses', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }
  public getLogins(path, body) {

    return this.http_.post(environment.host + path + '/getLogins', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }
  public getFileIntegrity(path, body) {

    return this.http_.post(environment.host + path + '/getFileIntegrity', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }

  public getFailedSSH(path, body) {
    return this.http_.post(environment.host + path + '/getFailedSSH', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }

  public getDiskIO(path, body) {

    return this.http_.post(environment.host + path + '/getDiskIO', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }

  public getNetflows(path, body) {

    // if(body.id == 'aegis-it-research-server1') body['ip'] = "78.46.102.90"
    // console.log("getCaseervice userId: ", userId);
    return this.http_.post(environment.host + path + '/getNetflows', body, { headers: this.myhttpHeaders }).toPromise()
  }

  public getNetworkIn(path, body) {

    return this.http_.post(environment.host + path + '/getNetworkIn', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }

  public getNetworkOut(path, body) {

    return this.http_.post(environment.host + path + '/getNetworkOut', body, { headers: this.myhttpHeaders }).toPromise().catch((error) => {
      const customError = { message: 'An error occurred while fetching data' };
      return customError;
    })
  }

  // public getAllPreconfViews() {
  //   return this.http_.get('assets/preconfViews').toPromise()
  // }



  // --- NEMO services here --- //
  


}
