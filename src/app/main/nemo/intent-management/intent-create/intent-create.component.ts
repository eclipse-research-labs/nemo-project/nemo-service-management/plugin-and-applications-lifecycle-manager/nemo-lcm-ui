import { Component, OnInit } from '@angular/core';

import { NodejsDataService } from '../../../nodejs-data.service';        // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, FormArray,
  Validators, AbstractControl, ValidatorFn, ValidationErrors} from "@angular/forms";   // Form creation and manipulation
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-intent-create',
  templateUrl: './intent-create.component.html',
  styleUrls: ['./intent-create.component.scss']
})
export class IntentCreateComponent implements OnInit {
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<IntentCreateComponent>,
    private snackBar: MatSnackBar
  ) {
    this.createForms();
  }

  async ngOnInit(): Promise<void> {
    await this.getIntentTypes();        // Intents Create - Form
    this.createForms();                 // Intents Create - Form
  }

  async getIntentTypes(): Promise<void> {
    // Get intents list
    let result = await this.nodejsService.getIntentTypes();
    this.intentTypesOptions = result.types;
  }

  // Intents Form - START
  createIntentForm: UntypedFormGroup;
  intentTypesOptions = [];
  target_condition_Options = [
    'IS_EQUAL_TO',
    'IS_LESS_THAN',
    'IS_GREATER_THAN',
    'IS_WITHIN_RANGE', 
    'IS_OUTSIDE_RANGE', 
    'IS_ONE_OF',
    'IS_NOT_ONE_OF',
    'IS_EQUAL_TO_OR_LESS_THAN',
    'IS_EQUAL_TO_OR_GREATER_THAN',
    'IS_ALL_OF'
  ]
  incorrect_form_messages = {
    workload_instance_id: [
      { type: "required", message: "Workload Instance ID is required"},
    ],
    intent_type: [
      { type: "required", message: "Intent Type is required"}
    ],
    target_name: [
      { type: "required", message: "At least one target is required"}
    ],
    targets: [
      { type: "incomplete", message: "ALL or NONE fields of the target must be filled."}
    ]
  };

  createForms(){
    this.createIntentForm = this.fb.group({
      workload_instance_id: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("")
        ])
      ),
      intent_type: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
      start_time: new UntypedFormControl(
        null, 
      ),
      end_time: new UntypedFormControl(
        null, 
      ),
      //targets
      targets: this.fb.array([
        this.fb.group({
          target_name: [''],
          target_condition: [''],
          target_value: [''],
        }, { validators: this.incompleteTargetValidator() })
      ]),
    });
  }

  async submitForm(form_values){
    if (this.createIntentForm.invalid) {
      this.createIntentForm.markAllAsTouched();  // Simulates the user interacting with all form controls triggering any validation messages
      return;
    }
    else{
      this.spinner.show('intents');

      // Create Intent API Service
      let recon_form_values = {
        instance_id: form_values.workload_instance_id,
        intent_type: form_values.intent_type,
        service_start_time: form_values.start_time,
        service_end_time: form_values.end_time,
        targets: form_values.targets.map(target => ({
          target_name: target.target_name,
          target_condition: target.target_condition,
          target_value_range: target.target_value
        }))
      }
      let result = await this.nodejsService.createIntent(recon_form_values)

      this.spinner.hide('intents');

      // Success and Error Behavior
      if (result && result.message && result.message.error) {  // Print error
        for (let err in result.message.error){
          this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
        }
      }
      else {    // Close modal
        this.dialogRef.close(true);
      }
    }
  }

  resetForm(){
    // Remove extra form fields
    while (this.targets.length > 1) {
      this.targets.removeAt(1);
    }
    // Clear input
    this.createIntentForm.reset();

    return;  
  }

  get targets(): FormArray {
    return this.createIntentForm.get('targets') as FormArray;
  }

  addFormField(){
    this.targets.push(
      this.fb.group({
        target_name: [''],
        target_condition: [''],
        target_value: [''],
      }, { validators: this.incompleteTargetValidator() })
    )
    return
  }

  incompleteTargetValidator(): ValidatorFn {
    return (group: AbstractControl): ValidationErrors | null => {
      const targetName = group.get('target_name').value;
      const targetCondition = group.get('target_condition').value;
      const targetValue = group.get('target_value').value;
  
      const isInvalid = (targetName || targetCondition || targetValue) &&
                        (!targetName || !targetCondition || !targetValue);
      return isInvalid ? { incompleteTargetValidator: true } : null;
    };
  }
  // Intents Form - END
}
