import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { NodejsDataService } from '../../../nodejs-data.service';    // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
    selector: 'app-intent-report',
    templateUrl: './intent-report.component.html',
    styleUrls: ['./intent-report.component.scss']
})
export class IntentReportComponent implements OnInit {
    intent: any;
    intent_data;
    targets_disabled = [];

    constructor(
        public dialogRef: MatDialogRef<IntentReportComponent>,
        @Inject(MAT_DIALOG_DATA) data: any,
        private nodejsService: NodejsDataService,
        private spinner: NgxSpinnerService,
        private snackBar: MatSnackBar,
    ){
        this.intent = data.intent;
        console.log()
        this.intent_data = {
            id: this.intent.id,
            name: this.intent.user_label,
            feasibility: this.intent.intent_report_reference.intent_feasibility_check_report.feasibility_check_type,
            infeasibility_reason: this.intent.intent_report_reference.intent_feasibility_check_report?.infeasibility_reason ?? null,
            fulfilment_status: this.intent.intent_report_reference.intent_fulfilment_report.intent_fulfilment_info.fulfilment_status,
            not_fulfilled_state: this.intent.intent_report_reference.intent_fulfilment_report.intent_fulfilment_info?.not_fulfilled_state ?? null,
            last_updated_time: this.intent.intent_report_reference.last_updated_time,
            target_fulfilment_results: this.intent.intent_report_reference.intent_fulfilment_report.expectation_fulfilment_results[0].target_fulfilment_results
        }
    }

    ngOnInit() {
        this.targets_disabled = this.intent.intent_expectations[0].expectation_targets.map(() => true);
    }

    getTargetTooltip(index: number): string {
        const target = this.intent_data.target_fulfilment_results[index]?.target || '-';
        const targetAchievedValue = this.intent_data.target_fulfilment_results[index]?.target_achieved_value || '-';
        const not_fulfilled_state = this.intent_data.target_fulfilment_results[index]?.target_fulfilment_info?.not_fulfilled_state || '-';
      
        return `ID: ${target}\nAchieved Value: ${targetAchievedValue}\nNot Fulfilled State: ${not_fulfilled_state}`;
    }
    
    async suspendIntent(id){
        // Spinner
        this.spinner.show('intent_report');
    
        // Create data structure
        let data = {
          action: "suspend",
        }
    
        // Call API
        let result = await this.nodejsService.actionIntent(id, data)
    
        // Handle response
        if (result && result.message && result.message.error) {   // Print error
          for (let err in result.message.error){
            this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
          }
        }
        else {                                                    // Print success
          this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
        }
    
        // Spinner
        this.spinner.hide('intent_report');
        return
    }
    
    async resumeIntent(id){
        // Spinner
        this.spinner.show('intent_report');
    
        // Create data structure
        let data = {
          action: "resume",
        }
        
        // Call API
        let result = await this.nodejsService.actionIntent(id, data)
    
        // Handle response
        if (result && result.message && result.message.error) {   // Print error
          for (let err in result.message.error){
            this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
          }
        }
        else {                                                    // Print success
          this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
        }
    
        // Spinner
        this.spinner.hide('intent_report');
        return
    }
    
    async terminateIntent(id){
        // Spinner
        this.spinner.show('intent_report');
    
        // Create data structure
        let data = {
          action: "terminate"
        }
    
        // Call API
        let result = await this.nodejsService.actionIntent(id, data)
    
        // Handle response
        if (result && result.message && result.message.error) {   // Print error
          for (let err in result.message.error){
            this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
          }
        }
        else {                                                    // Print success
          this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
        }
    
        // Spinner
        this.spinner.hide('intent_report');
        return
    }

    editTarget(target, index){
      this.targets_disabled[index] = !this.targets_disabled[index]
    }

    async saveTarget(targer, index, event){
		const new_value = event.target.closest('.row').querySelector('input[name="target_value_range"]').value;
		// console.log(new_value)
		// return

        // Spinner
        this.spinner.show('intent_report');
      
        // Create data structure
        let data = {
          "target_id": targer.id,
		      "target_value_range": new_value
        }
    
        // Call API
        let result = await this.nodejsService.changeTargetIntent(this.intent_data.id, data)
    
        // Handle response
        if (result && result.message && result.message.error) {   // Print error
          for (let err in result.message.error){
            this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
          }
        }
        else {                                                    // Print success
          this.snackBar.open("Successfully Updated Target", null, { duration: 3000, panelClass: 'errorClass' });
        }
    
        // Spinner
        this.spinner.hide('intent_report');
        return
    }

}