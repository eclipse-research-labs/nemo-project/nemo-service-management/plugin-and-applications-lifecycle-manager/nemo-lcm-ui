import { Component, OnInit, ViewChild } from '@angular/core';     // Basic angular

import { NodejsDataService } from '../../nodejs-data.service';    // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { MatTableDataSource } from "@angular/material/table";     // Crud Table
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { MatDialog, MatDialogRef } from "@angular/material/dialog";     // Popups
import { IntentCreateComponent } from "./intent-create/intent-create.component";
import { IntentReportComponent } from "./intent-report/intent-report.component";

@Component({
  selector: 'app-intent-management',
  templateUrl: './intent-management.component.html',
  styleUrls: ['./intent-management.component.scss']
})
export class IntentManagementComponent implements OnInit {
  dialog_createIntent_ref: MatDialogRef<IntentCreateComponent>;
  dialog_intentReport_ref: MatDialogRef<IntentReportComponent>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    public matDialog: MatDialog,
    private snackBar: MatSnackBar
  ){
  }

  async ngOnInit(): Promise<void> {
    // Setup worklaod data
    this.dataSourceIntents.data = [];   // Intents CRUD - Table
    this.getIntents();                  // Intents CRUD - Table
  }
  ngAfterViewInit() {
    this.dataSourceIntents.paginator = this.paginator;     // Intents CRUD - Paginator
    this.dataSourceIntents.sort = this.sort;               // Intents CRUD - Sort
  }

  async getIntents(): Promise<void> {
    this.spinner.show('intents');

    // Get intents list
    let result = await this.nodejsService.getIntents();

    // Check rights

    
    // Transform data for the table
    this.dataSourceIntents.data = result.map(intent => ({
      id: intent.id,
      name: intent.user_label,
      workload_instance_id: intent.intent_expectations[0].expectation_object.object_instance,
      fulfilment_status: intent.intent_report_reference.intent_fulfilment_report.intent_fulfilment_info.fulfilment_status,
      not_fulfilled_state: intent.intent_report_reference.intent_fulfilment_report.intent_fulfilment_info?.not_fulfilled_state ?? null,
      whole_intent: intent
    }))

    this.spinner.hide('intents');
  }

  // Intents Table - START
  displayedColumns: string[] = [
    "id",
    "status",
    "name",
    "workload_instance_id",
    "actions",
  ];
  dataSourceIntents = new MatTableDataSource();

  applySearch(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceIntents.filter = filterValue.trim().toLowerCase();
  }

  async suspendIntent(id){
    // Spinner
    this.spinner.show('workloads');

    // Create data structure
    let data = {
      action: "suspend",
    }

    // Call API
    let result = await this.nodejsService.actionIntent(id, data)

    // Handle response
    if (result && result.message && result.message.error) {   // Print error
      for (let err in result.message.error){
        this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
      }
    }
    else {                                                    // Print success
      this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
    }

    // Spinner
    this.spinner.hide('workloads');
    return
  }

  async resumeIntent(id){
    // Spinner
    this.spinner.show('workloads');

    // Create data structure
    let data = {
      action: "resume",
    }
    
    // Call API
    let result = await this.nodejsService.actionIntent(id, data)

    // Handle response
    if (result && result.message && result.message.error) {   // Print error
      for (let err in result.message.error){
        this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
      }
    }
    else {                                                    // Print success
      this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
    }

    // Spinner
    this.spinner.hide('workloads');
    return
  }

  async terminateIntent(id){
    // Spinner
    this.spinner.show('workloads');

    // Create data structure
    let data = {
      action: "terminate"
    }

    // Call API
    let result = await this.nodejsService.actionIntent(id, data)

    // Handle response
    if (result && result.message && result.message.error) {   // Print error
      for (let err in result.message.error){
        this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
      }
    }
    else {                                                    // Print success
      this.snackBar.open("Successful Termination", null, { duration: 3000, panelClass: 'errorClass' });
    }

    // Spinner
    this.spinner.hide('workloads');
    return
  }

  async intentReport(intent){
    // Get Info

    // Open modal
    this.dialog_intentReport_ref = this.matDialog.open(IntentReportComponent, {
      autoFocus: false,
      width: '60%',
      data: {
        intent: intent
      }
    });
    this.dialog_intentReport_ref.afterClosed().subscribe((res) => {
      // Do nothing
    });
  }

  // Intents Table - END

  // Intents Form - START
  createIntent(){
    this.dialog_createIntent_ref = this.matDialog.open(IntentCreateComponent, {
      width: '60%',
    })

    this.dialog_createIntent_ref.afterClosed().subscribe((res) => {
      if (res == true) {
        this.getIntents();     // Update the table
        this.snackBar.open("Created Cluster Successfully", null, { duration: 3000, panelClass: 'successClass' }); // Snackbar success
      }
    });
  }
  // Intents Form - END

  
  // Helpers //
  copyToClipboard(tag){
    const input = document.createElement('input');
    input.value = tag; // Set the full string to the input's value
    document.body.appendChild(input); // Append it to the document body
    input.select(); // Select the text
    document.execCommand('copy'); // Copy the text to the clipboard
    document.body.removeChild(input);
  }
}
