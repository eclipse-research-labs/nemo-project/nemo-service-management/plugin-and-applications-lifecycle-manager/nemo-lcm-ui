import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

//import { NodejsDataService } from '../../../nodejs-data.service';        // Basic Services
//import { NgxSpinnerService } from 'ngx-spinner';
//import { MatSnackBar } from "@angular/material/snack-bar";
//import * as yaml from 'js-yaml';

// import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, FormArray,
//     Validators, AbstractControl, ValidatorFn, ValidationErrors} from "@angular/forms";  



@Component({
    selector: 'app-instance-manifests',
    templateUrl: './instance-manifests.component.html',
    styleUrls: ['./instance-manifests.component.scss']
})
export class InstanceManifestsComponent implements OnInit {
    data: any;
    manifests;

    constructor(
        public dialogRef: MatDialogRef<InstanceManifestsComponent>,
        @Inject(MAT_DIALOG_DATA) data: any,
        //private nodejsService: NodejsDataService,
        //private spinner: NgxSpinnerService,
        //private snackBar: MatSnackBar,
    ){
        this.data = data;
    }

    ngOnInit() {
    }


    // Helpers //
    copyToClipboard() {
        navigator.clipboard.writeText(this.data.manifests).then(
            (res) => {},
            (err) => {}
        );
    }
}