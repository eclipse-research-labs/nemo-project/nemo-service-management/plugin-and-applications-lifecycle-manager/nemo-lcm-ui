import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkloadInstancesComponent } from './workload-instances.component';

describe('WorkloadInstancesComponent', () => {
  let component: WorkloadInstancesComponent;
  let fixture: ComponentFixture<WorkloadInstancesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkloadInstancesComponent]
    });
    fixture = TestBed.createComponent(WorkloadInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
