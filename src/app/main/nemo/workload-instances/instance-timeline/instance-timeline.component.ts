import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";


@Component({
    selector: 'app-instance-timeline',
    templateUrl: './instance-timeline.component.html',
    styleUrls: ['./instance-timeline.component.scss']
})
export class InstanceTimelineComponent implements OnInit {
    data: any;
    events: any;
    timelineEvents;

    constructor(
        public dialogRef: MatDialogRef<InstanceTimelineComponent>,
        @Inject(MAT_DIALOG_DATA) data: any,
        //private nodejsService: NodejsDataService,
        //private spinner: NgxSpinnerService,
        //private snackBar: MatSnackBar,
    ){
        this.data = data;
        this.events = data.instance.wi_lifecycle_events
    }

    ngOnInit() {
        this.timelineEvents = this.events.map(event => ({
            id: event.id,
            timestamp: new Date(event.timestamp),
            title: `${event.type}`,
            description: `
                ${event.deployment_cluster ? `Deployment_Cluster: ${event.deployment_cluster}, ` : ""}
                ${event.migration_from_cluster ? `Migrated_From: ${event.migration_from_cluster}, ` : ""}
                ${event.migration_to_cluster ? `Migrated_To: ${event.migration_to_cluster}` : ""}
            `.trim()
          }));
    }

    


}


/* Example timeline event
NgxTimelineEvent {
    timestamp?: Date;
    title?: string;
    description?: string;
    id?: any;
    itemPosition?: NgxTimelineItemPosition;
}
*/
