import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NodejsDataService } from '../../nodejs-data.service';          // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";
import * as yaml from 'js-yaml';

import { MatTableDataSource } from "@angular/material/table";           // Crud Table
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { MatDialog, MatDialogRef } from "@angular/material/dialog";     // Popups
import { WorkloadDeleteConfirmationComponent } from "../workload-crud/workload-delete-confirmation/workload-delete-confirmation.component"
import { InstanceManifestsComponent } from "./instance-manifests/instance-manifests.component";
import { InstanceTimelineComponent } from "./instance-timeline/instance-timeline.component";

@Component({
  selector: 'app-workload-instances',
  templateUrl: './workload-instances.component.html',
  styleUrls: ['./workload-instances.component.scss']
})
export class WorkloadInstancesComponent implements OnInit {
  filter_type;
	filter_value;
	dialog_delConf_ref: MatDialogRef<WorkloadDeleteConfirmationComponent>;
	dialog_showManifests_ref: MatDialogRef<InstanceManifestsComponent>;
	dialog_Timeline_ref: MatDialogRef<InstanceTimelineComponent>;

	@ViewChild('paginator_instances') paginator_instances: MatPaginator;
	@ViewChild('sort_instances') sort_instances: MatSort;
	constructor(
		private route: ActivatedRoute,
		private nodejsService: NodejsDataService,
		private spinner: NgxSpinnerService,
		public matDialog: MatDialog,
		private snackBar: MatSnackBar,
		//private keycloakService: KeycloakService
	){
	}

	ngOnInit(): void {
		this.filter_type = this.route.snapshot.paramMap.get('ft');
		this.filter_value = this.route.snapshot.paramMap.get('fv');
		// Setup worklaod data
		this.getWorkloadInstances();
	}
	ngAfterViewInit() {
    this.workloadInstancesTable.paginator = this.paginator_instances;
    this.workloadInstancesTable.sort = this.sort_instances;
  }

	async getWorkloadInstances(): Promise<void> {
    this.spinner.show('workloads');

    // Get workload list
		let result;
		if(this.filter_value){
			result = await this.nodejsService.getWorkloadInstancesFiltered(this.filter_value);
		}
		else{
			result = await this.nodejsService.getWorkloadInstances();
		}

    // Transform data for the table
    this.workloadInstancesTable.data = result.map(instance => ({
      wi_id: instance.id,
      wi_instance_id: instance.instance_id,
      wi_release_name: instance.release_name,
      wi_workload_document_id: instance.workload_document,
      wi_status: instance.status,
      wi_cluster_name: instance.cluster_name,
      wi_actions: "view",
      wi_lifecycle_events: instance.lifecycle_events
    }))

    this.spinner.hide('workloads');
  }

	workloadInstancesTable = new MatTableDataSource();
	displayedColumns_instances: string[] = [
		"wi_id",
		"wi_release_name",
		"wi_workload_document_id",
		"wi_status",
		"wi_cluster_name",
		"wi_instance_id",
		"wi_actions"
	];
	
	applySearch_instances(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.workloadInstancesTable.filter = filterValue.trim().toLowerCase();
	}
	
	async DeleteInstance(id, instance_id) {
		this.dialog_delConf_ref = this.matDialog.open(WorkloadDeleteConfirmationComponent, {
			data: {
				type: 'instance',
				id: id
			}
		})
		this.dialog_delConf_ref.afterClosed().subscribe((res) => {
			if (res == true) {
				this.spinner.show('workloads');
				this.nodejsService.deleteWorkloadInstance(instance_id).then(() => {
					this.spinner.hide('workloads');
					this.getWorkloadInstances();     // Update the table
					this.snackBar.open("Workload Instance Deleted Successfully", null, { duration: 3000, panelClass: 'successClass' });
				}).catch(error => {
					this.spinner.hide('workloads');
					console.log(error)
				});
			}
		});

	}

	async showInstanceManifests(id, instance_id){
		// Get Manifests
		this.spinner.show('workloads');
		let result = await this.nodejsService.getInstanceManifests(instance_id);
		let result_yaml = yaml.dump(result)
		this.spinner.hide('workloads');

		// Open modal
		this.dialog_showManifests_ref = this.matDialog.open(InstanceManifestsComponent, {
			width: '60%',
			data: {
				manifests: result_yaml,
				instance_id: instance_id,
				id: id
			}
		});
		this.dialog_showManifests_ref.afterClosed().subscribe((res) => {
			// Do nothing
		});
	}

	async showInstanceTimeline(instance_element){
		// Get Info
		let instance = instance_element

		// Open modal
		this.dialog_Timeline_ref = this.matDialog.open(InstanceTimelineComponent, {
			width: '60%',
			data: {
				instance: instance,
			}
		});
		this.dialog_Timeline_ref.afterClosed().subscribe((res) => {
			// Do nothing
		});
	}
	// Workload Instances Table - END
	

	// Helpers //
	copyToClipboard(tag){
		const input = document.createElement('input');
		input.value = tag; // Set the full string to the input's value
		document.body.appendChild(input); // Append it to the document body
		input.select(); // Select the text
		document.execCommand('copy'); // Copy the text to the clipboard
		document.body.removeChild(input);
	}

}
