import { Component, OnInit, Inject } from '@angular/core';
import { SocketioService } from '../../../socketio.service';
import { ApexAxisChartSeries, ApexChart, ApexXAxis, ApexStroke } from 'ng-apexcharts';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  colors: string[];
  title: {
    text: string;
    align: string;
  };
};

// import { NodejsDataService } from '../../../nodejs-data.service';        // Basic Services
// import { NgxSpinnerService } from 'ngx-spinner';
// import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-cluster-info',
  templateUrl: './cluster-info.component.html',
  styleUrls: ['./cluster-info.component.scss']
})
export class ClusterInfoComponent implements OnInit {
  chartOptions: Partial<ChartOptions>;
  cpuOptions: Partial<ChartOptions>;
  memoryOptions: Partial<ChartOptions>;
  diskOptions: Partial<ChartOptions>;

  // Temp Historic Data values
  cpuUsage: { name: string; data: number[] } = { name: 'CPU Usage', data: [
    5631.616, 5856.539, 5522.183, 5342.488, 4517.71,  5575.822, 4113.169, 4480.434, 
    5378.4,   5740.534, 5104.856, 5254.713, 5861.72,  5484.989, 5630.47,  5043.43,  
    4815.72,  5209.566, 5351.976, 4602.072, 4046.246, 4006.873, 4782.358, 5405.249, 
    5668.282, 4368.646, 4730.54,  4140.183, 4877.809, 5038.227, 5642.629, 5064.939, 
    4958.45,  5217.568, 5214.247, 5850.417, 4917.467, 5007.039, 4172.419, 4577.254, 
    4391.836, 5723.473, 5791.439, 4827.213, 4798.237, 5219.933, 4523.287, 4952.177, 
    4627.706, 5099.072, 4569.362, 4326.859, 5611.972, 5804.944, 5571.754, 4174.261, 
    5800.17,  4076.777, 4977.504, 5818.482, 5726.767, 4629.643, 4428.386, 5963.625, 
    4685.859, 5061.055, 4779.052, 5483.044, 4079.858, 5350.756, 4074.419, 5407.618, 
    4070.188, 4437.481, 4516.373, 4982.571, 4249.257, 4186.841, 4753.205, 5973.763, 
    5909.482, 5825.018, 5573.303, 4725.528, 4936.871, 5745.071, 4384.267, 5670.134, 
    5716.938, 4861.048, 5098.397, 5060.074, 5410.595, 5211.281, 5032.322, 5537.051, 
    4911.224, 4174.389, 4271.637, 4045.877]
  };
  memoryUsage: { name: string; data: number[] } = {name: 'Memory Usage', data: [
      75.8, 76.2, 76.1, 76.3, 76.8, 75.4, 75.5, 75.5, 75.0, 76.6, 75.5, 76.9, 76.7, 76.0, 
      75.9, 75.9, 76.0, 76.5, 75.2, 75.9, 75.7, 76.2, 75.5, 75.7, 76.8, 76.9, 76.3, 76.3, 
      76.7, 76.7, 76.1, 76.8, 75.1, 76.5, 75.3, 75.7, 76.9, 76.0, 76.2, 76.2, 75.1, 75.9, 
      75.1, 75.9, 75.1, 76.4, 75.5, 75.8, 76.9, 76.0, 77.0, 75.7, 75.5, 75.7, 76.3, 75.7, 
      76.0, 75.5, 75.2, 75.1, 75.7, 75.4, 76.1, 77.0, 76.1, 75.5, 75.5, 76.0, 75.7, 76.3, 
      75.6, 76.5, 75.4, 76.7, 76.7, 75.2, 75.2, 77.0, 75.5, 76.9, 75.7, 75.5, 75.9, 76.1, 
      75.8, 75.4, 76.8, 75.3, 76.6, 75.1, 76.2, 76.4, 75.0, 75.3, 75.3, 76.9, 76.8, 76.4, 
      76.8, 76.0]
  };
  diskUsage: { name: string; data: number[] } = { name: 'Disk Usage',
    data: Array(100).fill(1.5)
  };
  timestamps = this.generateTimeArray(100);


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { clusterId: string },
    private socketServ:  SocketioService,
    // private nodejsService: NodejsDataService,
    // private spinner: NgxSpinnerService,
    // private snackBar: MatSnackBar
  ) {
    this.cpuOptions = {
      series: [
        {
          name: this.cpuUsage.name,
          data: this.cpuUsage.data
        }
      ],
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      stroke: {
        curve: 'smooth',
        width: 1
      },
      title: {
        text: 'CPU Usage (GHz)',
        align: 'left'
      },
      xaxis: {
        categories: this.timestamps,
        tickAmount: 20
      },
      colors: ['#007bff']
    };
    this.memoryOptions = {
      series: [
        {
          name: this.memoryUsage.name,
          data: this.memoryUsage.data
        }
      ],
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      stroke: {
        curve: 'smooth',
        width: 1
      },
      title: {
        text: 'Memory Usage (GB)',
        align: 'left'
      },
      xaxis: {
        categories: this.timestamps,
        tickAmount: 20
      },
      colors: ['#6f42c1'] // Custom colors (optional)
    };
    this.diskOptions = {
      series: [
        {
          name: this.diskUsage.name,
          data: this.diskUsage.data
        }
      ],
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      stroke: {
        curve: 'smooth',
        width: 1
      },
      title: {
        text: 'Disk Usage',
        align: 'left'
      },
      xaxis: {
        categories: this.timestamps,
        tickAmount: 20
      },
      colors: ['#28a745'] // Custom colors (optional)
    };
  }

  ngOnInit(): void {
    this.socketServ.rabbitmqMessages().subscribe(message => {
      // console.log(message)

      if(message){
        let relevantCluster_message = message.data.find(item => item.cluster === this.data.clusterId);
        // console.log(relevantCluster_message)

        // Update timestamps for all
        let timestamp = this.convertTimestampToLocalTime(relevantCluster_message.timestamp)
        this.timestamps.push(timestamp); //.split(" ")[1].split(".")[0]
        this.timestamps.shift();

        // Update CPU
        this.cpuUsage.data.push(relevantCluster_message.cpu_usage);
        this.cpuUsage.data.shift();
        this.cpuOptions.series = [
          {
            name: this.cpuUsage.name,
            data: [...this.cpuUsage.data]
          }
        ];

        // Update Memory
        this.memoryUsage.data.push(relevantCluster_message.memory_usage);
        this.memoryUsage.data.shift();
        this.memoryOptions.series = [
          {
            name: this.memoryUsage.name,
            data: [...this.memoryUsage.data]
          }
        ];

        // Update Disk
        this.diskUsage.data.push(relevantCluster_message.disk_usage);
        this.diskUsage.data.shift();
        this.diskOptions.series = [
          {
            name: this.diskUsage.name,
            data: [...this.diskUsage.data]
          }
        ];
        
      }
    })
  }



  // Temp functions for showcasing functionality
  convertTimestampToLocalTime(timestamp: string): string {
    // Parse the timestamp into a Date object (the ISO string includes timezone info)
    const date = new Date(timestamp);
  
    // Use Intl.DateTimeFormat to format the time in the local timezone
    const options: Intl.DateTimeFormatOptions = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false,
    };
  
    // Format the date to the local time
    const localTime = new Intl.DateTimeFormat('en-US', options).format(date);
    
    return localTime;
  }

  generateTimeArray(count: number): string[] {
    const times: string[] = [];
  
    let date = new Date(); // Get the current system time
  
    for (let i = 0; i < count; i++) {
      const hours = String(date.getHours()).padStart(2, '0');
      const minutes = String(date.getMinutes()).padStart(2, '0');
      const seconds = String(date.getSeconds()).padStart(2, '0');
      times.push(`${hours}:${minutes}:${seconds}`);
      date.setMinutes(date.getMinutes() - 1); // Subtract 1 minute
    }
  
    return times.reverse();
  }

}
