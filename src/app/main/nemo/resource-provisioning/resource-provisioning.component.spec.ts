import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceProvisioningComponent } from './resource-provisioning.component';

describe('ResourceProvisioningComponent', () => {
  let component: ResourceProvisioningComponent;
  let fixture: ComponentFixture<ResourceProvisioningComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ResourceProvisioningComponent]
    });
    fixture = TestBed.createComponent(ResourceProvisioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
