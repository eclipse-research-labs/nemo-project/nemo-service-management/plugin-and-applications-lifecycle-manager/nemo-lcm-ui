import { Component, OnInit } from '@angular/core';

import { NodejsDataService } from '../../../nodejs-data.service';        // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl,
  Validators} from "@angular/forms";                                    // Form creation and manipulation
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-cluster-create',
  templateUrl: './cluster-create.component.html',
  styleUrls: ['./cluster-create.component.scss']
})
export class ClusterCreateComponent implements OnInit {
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<ClusterCreateComponent>,
    private snackBar: MatSnackBar
  ) {
    this.createClusterForm();
  }

  ngOnInit(): void {
  }

  // Clusters Form - START
  clusterForm: UntypedFormGroup;
  incorrect_form_messages = {
    cluster_name: [
      { type: "required", message: "Cluster name is required"},
      { type: "maxlength", message: "Cluster name must not be more than 42 characters long"},
    ],
    cpus: [
      { type: "required", message: "Number of CPUs is required"},
      { type: "pattern", message: "CPUs must be an integer"}
    ],
    memory: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    storage: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    availability: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    green_energy: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    cost: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    cpu_base_rate: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ],
    memory_base_rate: [
      { type: "required", message: "Memory size is required"},
      { type: "pattern", message: "Memory size must be an integer"}
    ]
  }
  availability_options = [
    '99.9%', '99%', '90%'
  ];
  greenEnergy_options = [
    '0%', '20%', '40%', '60%', '80%', '100%'
  ];
  cost_options = [
    'low_cost', 'high_performance'
  ];

  createClusterForm(){
    this.clusterForm = this.fb.group({
      cluster_name: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(42),
          Validators.pattern(""),
        ])
      ),
      cpus: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[0-9]*$"),  // Must be integer
        ])
      ),
      memory: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[0-9]*$"),
        ])
      ),
      storage: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[0-9]*$"),
        ])
      ),
      availability: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
      green_energy: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
      cost: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
      cpu_base_rate: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
      memory_base_rate: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required
        ])
      ),
    });
  }

  async submitForm(form_values){
    if (this.clusterForm.invalid) {
      this.clusterForm.markAllAsTouched();  // Simulates the user interacting with all form controls triggering any validation messages
      return;
    }
    else{
      this.spinner.show('clusters');

      // Create Intent API Service
      let res = await this.nodejsService.createCluster(form_values)

      this.spinner.hide('clusters');

      // Success and Error Behavior
      if (res && res.message && res.message.error) {  // Print error
        for (let err in res.message.error){
          this.snackBar.open(res.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
        }
      }
      else {    // Close modal
        this.dialogRef.close(true);
      }
    }
  }

  resetForm(){
    // Clear input
    this.clusterForm.reset();

    return;  
  }
  // Clusters Form - END
}
