import { Component, OnInit, ViewChild } from '@angular/core';     // Basic angular

import { NodejsDataService } from '../../nodejs-data.service';    // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { MatTableDataSource } from "@angular/material/table";     // Crud Table
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { MatDialog, MatDialogRef } from "@angular/material/dialog";     // Popups
import { ClusterCreateComponent } from "./cluster-create/cluster-create.component";
import { ClusterInfoComponent } from "./cluster-info/cluster-info.component";

@Component({
  selector: 'app-resource-provisioning',
  templateUrl: './resource-provisioning.component.html',
  styleUrls: ['./resource-provisioning.component.scss']
})
export class ResourceProvisioningComponent implements OnInit {
  dialog_createCluster_ref: MatDialogRef<ClusterCreateComponent>;
  dialog_infoCluster_ref: MatDialogRef<ClusterInfoComponent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    public matDialog: MatDialog,
    private snackBar: MatSnackBar
  ){}

  ngOnInit(): void {
    this.dataSourceClusters.data = [];   // Cluster - Table
    this.getClusters();
  }

  ngAfterViewInit() {
    this.dataSourceClusters.paginator = this.paginator;     // Intents CRUD - Paginator
    this.dataSourceClusters.sort = this.sort;               // Intents CRUD - Sort
  }

  // Clusters Table - START
  dataSourceClusters = new MatTableDataSource();
  displayedColumns: string[] = [
    "ID",
    "vm_name",
    "Endpoint",
    "CPUs",
    "Storage",
    "Memory",
    "actions"
  ];

  applySearch(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceClusters.filter = filterValue.trim().toLowerCase();
  }

  async getClusters(): Promise<void> {
    this.spinner.show('clusters');

    // Get intents list
    let result = await this.nodejsService.getClusters();
    console.log(result)
    // Check rights
    
    // Transform data for the table
    this.dataSourceClusters.data = result.map(cluster => ({
      ID: cluster.id,
      vm_name: cluster.cluster_name,
      Endpoint: cluster.endpoint,
      CPUs: cluster.cpus,
      Storage: cluster.storage,
      Memory: cluster.memory
    }))

    this.spinner.hide('clusters');
  }

  open_cluster_dashboard(cluster_id){
    this.dialog_infoCluster_ref = this.matDialog.open(ClusterInfoComponent, {
      width: '70%',
      minHeight: '600px',
      data: { clusterId: cluster_id }
    })

    this.dialog_infoCluster_ref.afterClosed().subscribe((res) => {
      console.log('nothing')
    });
  }
  // Clusters Table - END


  // Clusters Form - START
  // Update table with  
  createCluster(){
    this.dialog_createCluster_ref = this.matDialog.open(ClusterCreateComponent, {
      width: '50%',
    })

    this.dialog_createCluster_ref.afterClosed().subscribe((res) => {
      if (res == true) {
        this.getClusters();     // Update the table
        this.snackBar.open("Created Cluster Successfully", null, { duration: 3000, panelClass: 'successClass' }); // Snackbar success
      }
    });
  }
  // Clusters Form - END
}
