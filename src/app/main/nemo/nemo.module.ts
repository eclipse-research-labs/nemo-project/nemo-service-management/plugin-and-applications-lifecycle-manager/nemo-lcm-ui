import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NemoRoutingModule } from './nemo-routing.module';
import { WorkloadCrudComponent } from './workload-crud/workload-crud.component';
import { WorkloadInstancesComponent } from './workload-instances/workload-instances.component';
import { IntentManagementComponent } from './intent-management/intent-management.component';
import { ResourceProvisioningComponent } from './resource-provisioning/resource-provisioning.component';
import { SharedModule } from 'src/app/shared/shared.module';

import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from '@angular/material/sort';

import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { MatChipsModule } from '@angular/material/chips';

import { NgxTimelineModule } from '@frxjs/ngx-timeline';

import { WorkloadRenderFormComponent } from './workload-crud/workload-render-form/workload-render-form.component';
import { WorkloadDeleteConfirmationComponent } from './workload-crud/workload-delete-confirmation/workload-delete-confirmation.component';
import { WorkloadCreateComponent } from './workload-crud/workload-create/workload-create.component';
import { InstanceManifestsComponent } from './workload-instances/instance-manifests/instance-manifests.component';
import { InstanceTimelineComponent } from './workload-instances/instance-timeline/instance-timeline.component';
import { ClusterCreateComponent } from './resource-provisioning/cluster-create/cluster-create.component';
import { ClusterInfoComponent } from './resource-provisioning/cluster-info/cluster-info.component';
import { IntentCreateComponent } from './intent-management/intent-create/intent-create.component';
import { IntentReportComponent } from './intent-management/intent-report/intent-report.component';


import { NgApexchartsModule } from 'ng-apexcharts';

@NgModule({
  declarations: [
    WorkloadCrudComponent,
    WorkloadInstancesComponent,
    IntentManagementComponent,
    ResourceProvisioningComponent,
    WorkloadRenderFormComponent,
    WorkloadDeleteConfirmationComponent,
    WorkloadCreateComponent,
    ClusterCreateComponent,
    ClusterInfoComponent,
    IntentCreateComponent,
    InstanceManifestsComponent,
    InstanceTimelineComponent,
    IntentReportComponent,
  ],
  imports: [
    CommonModule,
    NemoRoutingModule,
    SharedModule,

    MatTableModule,
    MatPaginatorModule,
    MatSortModule,

    FormsModule,
    ReactiveFormsModule,

    MatNativeDateModule,
    MatDatepickerModule,

    MatChipsModule,

    NgxTimelineModule,

    NgApexchartsModule,
  ]
})
export class NemoModule { }
