import { Component, OnInit } from '@angular/core';

import { NodejsDataService } from '../../../nodejs-data.service';        // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl,
  Validators} from "@angular/forms";                                    // Form creation and manipulation
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-workload-create',
  templateUrl: './workload-create.component.html',
  styleUrls: ['./workload-create.component.scss']
})
export class WorkloadCreateComponent implements OnInit {
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<WorkloadCreateComponent>,
    private snackBar: MatSnackBar
  ) {
    this.createWorkloadForm();
  }

  ngOnInit(): void {
    this.getIntentTypes();
  }

  // Create Workload - START
  workloadForm: UntypedFormGroup;
  incorrect_form_messages = {
    name: [
      { type: "required", message: "Workload Name is required"},
    ],
    version: [
      { type: "required", message: "Version is required"},
      { type: "pattern", message: "Version must be X.Y.Z format"}
    ],
    intents: [
      { type: "required", message: "Intents are required"},
    ]
  };
  type_options = ['chart']
  intentTypesOptions = [];
  selected_ingressSupport = false

  async getIntentTypes(): Promise<void> {
    // Get intents list
    let result = await this.nodejsService.getIntentTypes();
    this.intentTypesOptions = result.types;
    // let result = ['axaxa', 'test2', 'testststs']
    //this.intentTypesOptions = result;
  }

  createWorkloadForm(){
    this.workloadForm = this.fb.group({
      name: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.pattern(""),
        ])
      ),
      version: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(/^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$/),  // SemVer 2 Versioning
        ])
      ),
      intents: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(""),
        ])
      ),
      ingress_support: new UntypedFormControl(
        false
      )
    });
  }

  async submitWorkloadForm(values){
    if (this.workloadForm.invalid) {
      this.workloadForm.markAllAsTouched();  // Simulates the user interacting with all form controls triggering any validation messages
      return;
    }
    else{
      this.spinner.show('workloads');

      // Create Workload - API Service
      //let intentsArray: string[] = values.intents.split(";");
      let workload_values = {
        name: values.name,
        version: values.version,
        schema: {},
        intents: values.intents,
        type: "chart",
        ingress_support: values.ingress_support
      }

      let result = await this.nodejsService.createWorkload(workload_values)
      
      this.spinner.hide('workloads');

      // Success and Error Behavior
      if (result && result.message && result.message.error) {  // Print error
        for (let err in result.message.error){
          this.snackBar.open(result.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
        }
      }
      else {    // Close modal
        this.dialogRef.close(true);
      }
    }
  }

  resetWorkloadForm(){
    // Clear input
    this.workloadForm.reset();

    return;  
  }
  // Create Workload - END
}
