import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkloadCrudComponent } from './workload-crud.component';

describe('WorkloadCrudComponent', () => {
  let component: WorkloadCrudComponent;
  let fixture: ComponentFixture<WorkloadCrudComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkloadCrudComponent]
    });
    fixture = TestBed.createComponent(WorkloadCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
