import { Component, OnInit, Inject } from '@angular/core';

import { NodejsDataService } from '../../../nodejs-data.service';                       // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, FormArray,
  Validators, AbstractControl, ValidatorFn, ValidationErrors} from "@angular/forms";    // Form creation and manipulation
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { KeycloakService } from 'keycloak-angular';                                     // Keycloak


@Component({
  selector: 'app-workload-render-form',
  templateUrl: './workload-render-form.component.html',
  styleUrls: ['./workload-render-form.component.scss']
})
export class WorkloadRenderFormComponent implements OnInit {
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    private fb: UntypedFormBuilder,
    public dialogRef: MatDialogRef<WorkloadRenderFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar,
    private keycloakService: KeycloakService
  ) {
    this.createRenderForm();
  }

  async ngOnInit(): Promise<void>{
    await this.getIntentTypes();        // Intents Create - Form
  }

  // Keycloak
  hasRole(role: string): boolean {
    return this.keycloakService.isUserInRole(role);
  }

  // Create Workload Instace - START
  renderForm: UntypedFormGroup;
  intentTypesOptions = [];
  target_condition_Options = [
    'IS_EQUAL_TO',
    'IS_LESS_THAN',
    'IS_GREATER_THAN',
    'IS_WITHIN_RANGE', 
    'IS_OUTSIDE_RANGE', 
    'IS_ONE_OF',
    'IS_NOT_ONE_OF',
    'IS_EQUAL_TO_OR_LESS_THAN',
    'IS_EQUAL_TO_OR_GREATER_THAN',
    'IS_ALL_OF'
  ]
  incorrect_form_messages = {
    release_name: [
      { type: "required", message: "Release Name is required"},
    ]
  };

  async getIntentTypes(): Promise<void> {
    // Get intents list
    let result = await this.nodejsService.getIntentTypes();
    this.intentTypesOptions = result.types;
  }

  createRenderForm(){
    this.renderForm = this.fb.group({
      release_name: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.pattern(""),
        ])
      ),
      namespace: new UntypedFormControl(
        "default",
        Validators.compose([
          Validators.maxLength(100),
          Validators.pattern(""),
        ])
      ),
      cluster_name: new UntypedFormControl(
        "",
        Validators.compose([
          Validators.maxLength(100),
          Validators.pattern(""),
        ])
      ),
      ingress_enabled: new UntypedFormControl(false),
      values_override: new UntypedFormControl('{}'),
      include_crds: new UntypedFormControl(false),
      is_upgrade: new UntypedFormControl(false),
      no_hooks: new UntypedFormControl(false),
      // Intents
      intents: this.fb.array([]),
    });
  }

  async submitRenderForm(values){
    if (this.renderForm.invalid) {
      this.renderForm.markAllAsTouched();  // Simulates the user interacting with all form controls triggering any validation messages
      return;
    }
    else{
      this.spinner.show('workloads_render');

      // Create Workload - API Service
      let render_data: {
        release_name: any;
        cluster_name?: any;
        namespace: any;
        values_override: any;
        include_crds: any;
        is_upgrade: any;
        no_hooks: any;
        ingress_enabled: any;
        intents: any;
      } = {
        release_name: values.release_name,
        namespace: values.namespace,
        values_override: JSON.parse(values.values_override),
        include_crds: values.include_crds,
        is_upgrade: values.is_upgrade,
        no_hooks: values.no_hooks,
        ingress_enabled: values.ingress_enabled,
        intents: values.intents.map(intent =>({
          "intent_type": intent.intent_type,
          "service_start_time": new Date(intent.start_time).toISOString(),
          "service_end_time": new Date(intent.end_time).toISOString(),
          "targets": intent.targets.map(target =>({
            "target_name": target.target_name,
            "target_condition": target.target_condition,
            "target_value_range": target.target_value
          }))
        }))
      }
      if (values.cluster_name && values.cluster_name.trim() !== '') {
        render_data.cluster_name = values.cluster_name;
    }
      console.log(render_data)

      let res = await this.nodejsService.renderWorkload(render_data, this.data.id.id)

      this.spinner.hide('workloads_render');

      if (res.message && res.message.error) {  // Print error
        for (let err in res.message.error){
          this.snackBar.open(res.message.error[err], null, { duration: 3000, panelClass: 'errorClass' });
        }
      }
      else {    // Close modal
        this.dialogRef.close('suc');
      }      
    }
  }

  get intents(): FormArray{
    return this.renderForm.get('intents') as FormArray;
  }

  getTargets(intentIndex: number): FormArray {
    return this.intents.at(intentIndex).get('targets') as FormArray;
  }

  addIntentForm(){
    this.intents.push(
      this.fb.group({
        intent_type: [''],
        start_time: new UntypedFormControl(null,),
        end_time: new UntypedFormControl(null,),
        targets: this.fb.array([
          this.fb.group({
            target_name: [''],
            target_condition: [''],
            target_value: [''],
          }, {})
        ]),
      })
    )
    return
  }

  addTargetForm(intentIndex){
    const targets = this.getTargets(intentIndex);
    targets.push(
      this.fb.group({
        target_name: [''],
        target_condition: [''],
        target_value: [''],
      }, {})
    )
    return
  }

  removeIntentForm(intentIndex){
    this.intents.removeAt(intentIndex);
  }

  removeTargetForm(intentIndex, targetIndex){
    const targets = this.getTargets(intentIndex);
    targets.removeAt(targetIndex);
  }

  incompleteTargetValidator(): ValidatorFn {
    return (group: AbstractControl): ValidationErrors | null => {
      const targetName = group.get('target_name').value;
      const targetCondition = group.get('target_condition').value;
      const targetValue = group.get('target_value').value;
  
      const isInvalid = (targetName || targetCondition || targetValue) &&
                        (!targetName || !targetCondition || !targetValue);
      return isInvalid ? { incompleteTargetValidator: true } : null;
    };
  }

  resetRenderForm(){
    // Remove extra form fields
    while (this.intents.length > 0) {
      this.intents.removeAt(0);
    }
    // Clear input
    this.renderForm.reset();

    return;  
  }
  // Create Workload Instace - END
}
