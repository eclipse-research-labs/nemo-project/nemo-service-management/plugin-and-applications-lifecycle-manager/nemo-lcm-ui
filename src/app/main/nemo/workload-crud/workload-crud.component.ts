import { Component, OnInit, ViewChild } from '@angular/core';

import { NodejsDataService } from '../../nodejs-data.service';          // Basic Services
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from "@angular/material/snack-bar";

import { MatTableDataSource } from "@angular/material/table";           // Crud Table
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { MatDialog, MatDialogRef } from "@angular/material/dialog";     // Popups
import { WorkloadCreateComponent } from "./workload-create/workload-create.component";
import { WorkloadRenderFormComponent } from "./workload-render-form/workload-render-form.component";
import { WorkloadDeleteConfirmationComponent } from "./workload-delete-confirmation/workload-delete-confirmation.component"


//import { KeycloakService } from 'keycloak-angular';   // Keycloak info


@Component({
  selector: 'app-workload-crud',
  templateUrl: './workload-crud.component.html',
  styleUrls: ['./workload-crud.component.scss']
})
export class WorkloadCrudComponent implements OnInit {
  dialog_delConf_ref: MatDialogRef<WorkloadDeleteConfirmationComponent>;
  dialog_createWorkload_ref: MatDialogRef<WorkloadCreateComponent>;
  dialog_renderWorkload_ref: MatDialogRef<WorkloadRenderFormComponent>;

  @ViewChild('paginator_workloads') paginator_workloads: MatPaginator;
  @ViewChild('sort_workloads') sort_workloads: MatSort;
  @ViewChild('paginator_instances') paginator_instances: MatPaginator;
  @ViewChild('sort_instances') sort_instances: MatSort;
  constructor(
    private nodejsService: NodejsDataService,
    private spinner: NgxSpinnerService,
    public matDialog: MatDialog,
    private snackBar: MatSnackBar,
    //private keycloakService: KeycloakService
  ){
  }

  ngOnInit(): void {
    // Setup worklaod data
    this.workloadsTable.data = [];
    this.getWorkloads();
  }
  ngAfterViewInit() {
    this.workloadsTable.paginator = this.paginator_workloads;
    this.workloadsTable.sort = this.sort_workloads;
  }

  async getWorkloads(): Promise<void> {
    this.spinner.show('workloads');

    // Get workload list
    let res_ib = await this.nodejsService.getWorkloads();

    // // Get MOCA workload data
    // let res_moca = await this.nodejsService.getWorkloadsMoca();

    // // JOIN IB and Moca Api results
    // let result = res_ib.map(item => {
    //   let matchingMoca = res_moca.find(moca => moca.id === item.id);
    //   return {
    //       ...item,
    //       moca: matchingMoca || null // Add moca data if found, otherwise set to null
    //   };
    // });
    //console.log(result)
    // let result = res_ib

    // Temp bypass of moca
    let result;
    try{
      let res_moca = await this.nodejsService.getWorkloadsMoca();
      result = res_ib.map(item => {
        let matchingMoca = res_moca.find(moca => moca.id === item.id);
        return {
            ...item,
            moca: matchingMoca || null // Add moca data if found, otherwise set to null
        };
      });
    }
    catch (error){
      result = res_ib
    }

    // Check rights
    //const loggedInUser = this.keycloakService.getUsername();

    // Transform data for the table  
    this.workloadsTable.data = result.map(workload => ({
      id: workload.id, status: workload.status, name: workload.name, version: workload.version, user: workload.user,
      actions: "view",
      chart: workload.chart,
      ingress_support: workload.ingress_support,
      balance: workload.moca?.balance ?? '-'
    }))

    this.spinner.hide('workloads');
  }

  // Create Workload - START
  createWorkload(){
    this.dialog_createWorkload_ref = this.matDialog.open(WorkloadCreateComponent, {
      width: '50%',
    })

    this.dialog_createWorkload_ref.afterClosed().subscribe((res) => {
      if (res == true) {
        this.getWorkloads();     // Update the table
        this.snackBar.open("Created Cluster Successfully", null, { duration: 3000, panelClass: 'successClass' }); // Snackbar success
      }
    });
  }
  // Create Workload - END


  // Workloads Table - START
  displayedColumns: string[] = [
    "id",
    "name",
    "version",
    "status",
    "ingress_support",
    "user",
    "balance",
    "actions",
  ];
  workloadsTable = new MatTableDataSource();

  applySearch(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.workloadsTable.filter = filterValue.trim().toLowerCase();
  }

  uploaded_file;
  async UploadWorkloadDocument(event: any, name, version) {
    const file = event.target.files[0];
    if (file) {
      this.uploaded_file = file;
      
      this.spinner.show('workloads');

      // Upload workload document - API Service
      const formData = new FormData();
      formData.append('file', this.uploaded_file);
      formData.append('name', name);
      formData.append('version', version);

      await this.nodejsService.uploadWorkloadDocument(formData)

      // Update table dataSource
      this.getWorkloads();

      this.spinner.hide('workloads');
    }
  }

  async RenderWorkload(id){
    this.dialog_renderWorkload_ref = this.matDialog.open(WorkloadRenderFormComponent, {
      width: '60%',
      data: {id: id}
    })

    this.dialog_renderWorkload_ref.afterClosed().subscribe((res) => {
      if (res == 'suc'){
        this.snackBar.open("Workload Instance Created Successfully", null, { duration: 3000, panelClass: 'successClass' });
      }
    });
  }

  async DeleteWorkload(id) {
    this.dialog_delConf_ref = this.matDialog.open(WorkloadDeleteConfirmationComponent, {
      data: {
        type: 'workload',
        id: id
      }
    })

    this.dialog_delConf_ref.afterClosed().subscribe((res) => {
      if (res == true) {
        this.nodejsService.deleteWorkload(id).then(() => {
          // Success
        }).catch(error => {
          console.log(error)
        });
      }
    });

  }

  associatedInstances(id): void {
    const url = `assets-overview/assets-list/services/workload-instances/id/${id}`;  // This is your target URL
    const newTab = window.open(url, '_blank');  // Open in a new tab
    if (newTab) {
      newTab.focus();  // Focus on the new tab
    }
  }
  // Workloads Table - END


  

  // Helpers //
  copyToClipboard(tag){
    const input = document.createElement('input');
    input.value = tag; // Set the full string to the input's value
    document.body.appendChild(input); // Append it to the document body
    input.select(); // Select the text
    document.execCommand('copy'); // Copy the text to the clipboard
    document.body.removeChild(input);
  }


}