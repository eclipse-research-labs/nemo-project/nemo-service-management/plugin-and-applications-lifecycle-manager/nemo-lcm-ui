import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-workload-delete-confirmation',
  templateUrl: './workload-delete-confirmation.component.html',
  styleUrls: ['./workload-delete-confirmation.component.scss']
})
export class WorkloadDeleteConfirmationComponent {
  data: any;

  constructor(
    private _mdr: MatDialogRef<WorkloadDeleteConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.data = data;
  }
  
  CloseDialog() {
    this._mdr.close(false);
  }
}
