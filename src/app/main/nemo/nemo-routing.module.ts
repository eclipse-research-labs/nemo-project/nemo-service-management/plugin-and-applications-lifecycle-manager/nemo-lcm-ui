import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkloadCrudComponent } from './workload-crud/workload-crud.component';
import { WorkloadInstancesComponent } from './workload-instances/workload-instances.component';
import { IntentManagementComponent } from './intent-management/intent-management.component';
import { ResourceProvisioningComponent } from './resource-provisioning/resource-provisioning.component';
import { AuthGuard } from '../..//guard/auth.guard';  // Import the AuthGuard

const routes: Routes = [
  {  
    path: 'workload-monitoring', component: WorkloadCrudComponent, pathMatch: 'full',
    canActivate: [AuthGuard],
    data: { roles: ['nemo_consumer', 'nemo_partner', 'nemo_provider'] }
  },
  {  // Instances without parameters
    path: 'workload-instances', component: WorkloadInstancesComponent, pathMatch: 'full',
    canActivate: [AuthGuard],
    data: { roles: ['nemo_consumer', 'nemo_partner', 'nemo_provider'] }
  },
  {  // Instances with parameter
    path: 'workload-instances/:ft/:fv', component: WorkloadInstancesComponent, pathMatch: 'full',
    canActivate: [AuthGuard],
    data: { roles: ['nemo_consumer', 'nemo_partner', 'nemo_provider'] }
  },
  {  
    path: 'intent-management', component: IntentManagementComponent, pathMatch: 'full',
    canActivate: [AuthGuard],
    data: { roles: ['nemo_partner', 'nemo_provider'] }
  },
  {  
    path: 'resource-provisioning', component: ResourceProvisioningComponent, pathMatch: 'full',
    canActivate: [AuthGuard],
    data: { roles: ['nemo_partner', 'nemo_provider'] }
  },
  {
    path: 'dashboards',
    loadChildren: () => import('../core-dash/core-dash.module').then(m => m.CoreDashModule)
  },
  {
    path: '**', redirectTo: '/assets-overview/assets-list'
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NemoRoutingModule { }
