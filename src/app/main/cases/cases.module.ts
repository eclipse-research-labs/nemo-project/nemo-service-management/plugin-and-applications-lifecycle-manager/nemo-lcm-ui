import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasesRoutingModule } from './cases-routing.module';
import { CasesOverviewComponent } from './cases-overview/cases-overview.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    CasesOverviewComponent
  ],
  imports: [
    CommonModule,
    CasesRoutingModule,
    SharedModule
  ]
})
export class CasesModule { }
