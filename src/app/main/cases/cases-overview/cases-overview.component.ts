import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
//import { CasesOverviewService } from './cases-overview.service';
//import { MatSnackBar } from '@angular/material/snack-bar';
import { NodejsDataService } from '../../nodejs-data.service';
import { KeycloakService } from 'keycloak-angular';
import { SocketioService } from '../../socketio.service';

@Component({
  selector: 'app-cases-overview',
  templateUrl: './cases-overview.component.html',
  styleUrls: ['./cases-overview.component.scss']
})
export class CasesOverviewComponent implements OnInit {

  assets; //could be w/e
  recentSaved;
  token;
  keycloak_id;
  constructor(
    //private overviewService: CasesOverviewService,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog,
    //private _snackBar: MatSnackBar,
    private nodejsService: NodejsDataService,
    private keycloakService: KeycloakService,
    private socketServ:  SocketioService,
  ) {

    this.spinner.show('assets');
    this.token = this.nodejsService.getToken();
    //this.keycloak_id = this.nodejsService.getKeycloakId();

    // #TOUCH
    //this.getAssetsDetails(this.token, '/assets');
    this.assets = [
      {
        service_name: 'Workload Monitoring',
        button_name: 'Manage Workloads',
        route: 'workload-monitoring',
        roles: ['nemo_consumer', 'nemo_partner', 'nemo_provider']
      },
      {
        service_name: 'Workload Instances',
        button_name: 'Manage Instances',
        route: 'workload-instances',
        roles: ['nemo_consumer', 'nemo_partner', 'nemo_provider']
      },
      {
        service_name: 'Intent Management',
        button_name: 'Manage Intents',
        route: 'intent-management',
        roles: ['nemo_partner', 'nemo_provider']
      },
      {
        service_name: 'Resource Provisioning',
        button_name: 'Manage Resources',
        route: 'resource-provisioning',
        roles: ['nemo_partner', 'nemo_provider']
      },
    ]
    this.spinner.hide('assets');

    this.socketServ.disconSocket();
  }


  ngOnInit(): void {
    // this.socketServ.rabbitmqMessages().subscribe(message => {
    //   console.log(message)
    // })

  }

  hasRole(roles: string[]): boolean {
    // Check if user has at least one of the required roles
    return roles.some(role => this.keycloakService.isUserInRole(role));
  }
}



// // --- FVT OG ---
// async getAssetsDetails(token, path): Promise<void> {
//   //get the assets/cases or w/e (differates per project).
//   // #TOUCH
//   this.assets = await this.overviewService.getAssets(token, path, { "start": 1693603731000, "end": new Date(Date.now()).getTime() });
//   if (this.assets == 'http-error') {
//     this._snackBar.open('An error has occured while trying to retrieve cases. Please try again.', null, { duration: 3500, panelClass: 'errorClass' })
//   } else {
//     // console.log(this.assets);
//     this.recentSaved = this.assets.flatMap(asset => asset.savedViews).filter(v => v);
//     // this.assets = this.authRightsServ.checkCasesRights(this.assets);
//   }
//   this.spinner.hide('assets');
// }
