import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasesOverviewComponent } from './cases-overview/cases-overview.component';

const routes: Routes = [

  {
    path: '', component: CasesOverviewComponent, pathMatch: 'full' as any
  },
  {
    path: 'dashboards',
    loadChildren: () => import('../core-dash/core-dash.module').then(m => m.CoreDashModule)
  },
  {
    path: 'services',
    loadChildren: () => import('../nemo/nemo.module').then(m => m.NemoModule)
  },
  {
    path: '**', redirectTo: ''
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasesRoutingModule { }
