import { KeycloakService } from 'keycloak-angular';
import { environment } from '../../environments/environment'


export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {

    return () =>
        keycloak.init({
            config: {
                url: environment.keycloakURL,
                realm: 'nemo',
                clientId: 'lifecycle-manager',

                // url: "http://localhost:8080/auth",
                // realm: 'fvt',
                // clientId: 'fvt-test',

            },
            initOptions: {
                checkLoginIframe: true,
                checkLoginIframeInterval: 25
            },
            loadUserProfileAtStartUp: true
        });

}

