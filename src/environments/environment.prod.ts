export const environment = {
  production: true,

  host: window["env"]["host"] || "",
  moca: window["env"]["moca"] || "",
  socket_host: window["env"]["socket_host"] || "",
  keycloakURL: window["env"]["keycloakURL"] || ""



};
