(function (window) {
    window["env"] = window["env"] || {};

    // Environment variables
    // // url: 'https://fvt-demo.aegisresearch.eu:8444/auth'
    // // url: 'https://localhost:8080/auth'

    window["env"]["host"] = "https://intent-api.nemo.onelab.eu/api/v1" //"/api";
    window["env"]["moca"] = "https://intent-api.nemo.onelab.eu/moca/api/v1";
    window["env"]["socket_host"] = "http://nemo-lcm-backend-service:5000" //"http://localhost:5000";                // "http://nemo-lcm-backend-service:5000" //
    window["env"]["keycloakURL"] = "https://keycloak.nemo.onelab.eu";


})(this);

