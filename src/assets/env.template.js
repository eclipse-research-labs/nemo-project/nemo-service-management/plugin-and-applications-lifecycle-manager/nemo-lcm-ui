(function (window) {
    window.env = window.env || {};

    // Environment variables
    // window["env"]["host"] = "${SERV_HOST}";
    // window["env"]["socket_host"] = "${SOCKET_HOST}";
    // window["env"]["keycloakURL"] = "${KEYCLOAK_URL}";

    window["env"]["host"] = "https://intent-api.nemo.onelab.eu/api/v1" //"/api";
    window["env"]["moca"] = "https://intent-api.nemo.onelab.eu/moca/api/v1";
    window["env"]["socket_host"] = "http://localhost:8000";
    window["env"]["keycloakURL"] = "https://keycloak.nemo.onelab.eu"; //"https://fvt-demo.aegisresearch.eu:8444/auth";


})(this);

